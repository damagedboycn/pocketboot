package com.damagedboy.base.permission;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true,isolation = Isolation.DEFAULT,propagation = Propagation.REQUIRED)
public class PermissionServiceImpl implements IPermissionService {

}
