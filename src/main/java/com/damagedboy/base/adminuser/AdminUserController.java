package com.damagedboy.base.adminuser;

import com.damagedboy.base.po.AdminUserPO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.RollbackException;
import java.util.List;

@RestController
@RequestMapping("/admin/adminuser")
public class AdminUserController {
    @Autowired
    private IAdminUserSV adminUserSV;

    @GetMapping("/search")
    public Object search(String username, String phone) {
        List<AdminUserPO> list = adminUserSV.getAdminUserList(username, phone);
        return list;
    }

    @GetMapping("/detail/{id}")
    public Object detail(@PathVariable("id") Integer id) {
        return adminUserSV.getById(id);
    }

    @PostMapping("/save")
    public Object save(AdminUserPO po) {
        return adminUserSV.save(po);
    }

    @PostMapping("/del/{id}")
    public Object del(@PathVariable("id") Integer id) {
        return adminUserSV.delById(id);
    }

}
