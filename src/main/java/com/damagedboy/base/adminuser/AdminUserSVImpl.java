package com.damagedboy.base.adminuser;

import com.damagedboy.base.po.AdminUserPO;
import com.damagedboy.base.utils.MD5Tools;
import com.damagedboy.base.utils.dao.ICommonDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.RollbackException;
import java.util.List;

@Service
public class AdminUserSVImpl implements IAdminUserSV {
    @Autowired
    IAdminUserDAO adminUserDAO;
    @Autowired
    ICommonDao<AdminUserPO> commonDao;

    @Override
    public List<AdminUserPO> getAdminUserList(String username, String phone) {
        return adminUserDAO.selectByCondition(username, phone);
    }

    @Override
    public AdminUserPO getById(Integer id) {
        return commonDao.getById("base_admin_user", id, AdminUserPO.class);
    }

    @Override
    @Transactional(readOnly = false, rollbackFor = RollbackException.class)
    public int save(AdminUserPO adminUserPO) {
        try {
            Integer id = adminUserPO.getId();
            if (id == null) {
                adminUserPO.setPassword(MD5Tools.MD5("000000"));
                return commonDao.insert("base_admin_user", adminUserPO);
            }
            return 0;
        } catch (Exception e) {
            throw new RollbackException(e);
        }
    }

    @Override
    @Transactional(readOnly = false, rollbackFor = RollbackException.class)
    public int delById(Integer id) {
        try {
            return commonDao.delById("base_admin_user", id);
        } catch (Exception e) {
            throw new RollbackException(e);
        }
    }

    @Override
    public AdminUserPO getById2(Integer id) {
        return adminUserDAO.getUserById(id);
    }


}
