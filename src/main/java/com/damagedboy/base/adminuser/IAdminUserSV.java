package com.damagedboy.base.adminuser;

import com.damagedboy.base.po.AdminUserPO;

import java.util.List;

public interface IAdminUserSV {
    List<AdminUserPO> getAdminUserList(String username, String phone);

    AdminUserPO getById(Integer id);

    int save(AdminUserPO adminUserPO);

    int delById(Integer id);

    AdminUserPO getById2(Integer id);
}
