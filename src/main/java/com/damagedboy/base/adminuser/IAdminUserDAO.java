package com.damagedboy.base.adminuser;

import com.damagedboy.base.po.AdminUserPO;

import java.util.List;

public interface IAdminUserDAO {
    List<AdminUserPO> selectByCondition(String username, String phone);

    AdminUserPO getAdminUserByUserNameAndPassword(String username, String password);

    AdminUserPO getUserById(Integer id);
}
