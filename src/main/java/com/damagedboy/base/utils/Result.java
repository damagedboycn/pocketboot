package com.damagedboy.base.utils;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

public class Result implements Serializable{
    public static ResultVO ok() {
        ResultVO vo = new ResultVO();
        vo.setStatus("ok");
        vo.setMessage("成功");
        return vo;
    }

    public static ResultVO fail() {
        ResultVO vo = new ResultVO();
        vo.setStatus("fail");
        vo.setMessage("服务器内部错误");
        return vo;
    }

    public static ResultVO fail(String msg) {
        ResultVO vo = new ResultVO();
        vo.setStatus("fail");
        vo.setMessage(msg);
        return vo;
    }

    public static ResultVO ok(Object object) {
        ResultVO vo = new ResultVO();
        vo.setStatus("ok");
        vo.setMessage("成功");
        vo.setData(object);
        return vo;
    }
}

@Data
@ToString
class ResultVO extends Result implements Serializable{
    private String status;
    private String message;
    private Object data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
