package com.damagedboy.base.utils;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class ApiResult {
    private String code;
    private String returnMessage;
    private Object bean;
    private Object beans;
}
