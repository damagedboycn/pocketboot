package com.damagedboy.base.utils.dao;

import com.damagedboy.pocketbook.core.po.PocketBookPO;

import java.io.Serializable;

public interface ICommonDao<T> {
    int insert(String tableName, Object o);

    int delById(String tableName, Serializable id);

    T getById(String tableName, Integer id, Class c);
}
