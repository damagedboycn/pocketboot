package com.damagedboy.base.utils;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class Router implements Serializable{
    private String path;
    private String name;
    private String component;
}