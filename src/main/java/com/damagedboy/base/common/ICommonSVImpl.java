package com.damagedboy.base.common;

import com.damagedboy.base.adminuser.IAdminUserDAO;
import com.damagedboy.base.po.AdminUserPO;
import com.damagedboy.base.utils.dao.ICommonDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ICommonSVImpl implements ICommonSV {
    @Autowired
    private IAdminUserDAO adminUserDAO;

    @Override
    public AdminUserPO getAdminUserByUserNameAndPassword(String username, String password) {
        return adminUserDAO.getAdminUserByUserNameAndPassword(username, password);
    }
}
