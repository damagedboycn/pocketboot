package com.damagedboy.base.common;

import com.damagedboy.base.po.AdminUserPO;

public interface ICommonSV {
    AdminUserPO getAdminUserByUserNameAndPassword(String username, String password);
}
