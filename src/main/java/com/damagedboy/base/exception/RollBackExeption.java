package com.damagedboy.base.exception;

public class RollBackExeption extends RuntimeException {
    public RollBackExeption() {
        super();
    }

    public RollBackExeption(String message) {
        super(message);
    }

    public RollBackExeption(String message, Throwable cause) {
        super(message, cause);
    }

    public RollBackExeption(Throwable cause) {
        super(cause);
    }

    protected RollBackExeption(String message, Throwable cause,
                               boolean enableSuppression,
                               boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
