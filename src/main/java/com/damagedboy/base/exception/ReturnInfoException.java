package com.damagedboy.base.exception;

public class ReturnInfoException extends Exception {
    private String errorCode;
    private String message;

    public ReturnInfoException(String errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
        this.message = message;
    }

    public String getErrorCode() {
        return this.errorCode;
    }

    public String getMessage() {
        return this.message;
    }
}
