package com.damagedboy.base.po;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;

/**
 * @author 李文涛 on 2017/8/1.
 * @descrtion
 */
@Data
@ToString
public class MenusPO {
    private Integer id;
    private String name;
    private String path;
    private Integer parentId;
    private MenusPO parentMenu;
}
