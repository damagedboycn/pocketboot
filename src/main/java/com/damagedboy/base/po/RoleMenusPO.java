package com.damagedboy.base.po;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;

/**
 * @author 李文涛 on 2017/8/1.
 * @descrtion
 */
@Data
@ToString
@Entity
@Table(schema = "base_role_menus")
public class RoleMenusPO {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private Integer roleId;
    private Integer menusId;
}
