package com.damagedboy.pocketbook.web.controller;

import com.damagedboy.base.adminuser.IAdminUserSV;
import com.damagedboy.base.exception.ReturnInfoException;
import com.damagedboy.base.po.AdminUserPO;
import com.damagedboy.pocketbook.core.po.PocketBookPO;
import com.damagedboy.pocketbook.core.service.IPocketBookSV;
import com.damagedboy.pocketbook.utils.BatchUilts;
import com.damagedboy.pocketbook.utils.beans.PocketBatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/admin/pocketbook")
public class PocketBookController {
    @Autowired
    private IPocketBookSV pocketBookSV;

    @Autowired
    private IAdminUserSV adminUserSV;

    @GetMapping("/search")
    public Object search(String findTime, Integer createId, HttpServletRequest request) throws ReturnInfoException {
        HttpSession session = request.getSession();
        AdminUserPO sessionuser = (AdminUserPO) session.getAttribute("adminusersession");
        if (sessionuser == null) {
            throw new ReturnInfoException("-1", "session lose");
        }
        Integer id = sessionuser.getId();
        if (createId == null) {
            String name = sessionuser.getName();
            if (!name.equals("admin")) {
                createId = id;
            }
        }

        List<PocketBookPO> list = pocketBookSV.getList(findTime, createId);
        return list;
    }

    @GetMapping("/detail/{id}")
    public Object detail(@PathVariable("id") Integer id) {
        return pocketBookSV.getById(id);
    }

    @PostMapping("/save")
    public Object save(PocketBookPO po, HttpServletRequest request) throws ReturnInfoException {
        if (StringUtils.isEmpty(po.getParticipant())) {
            throw new ReturnInfoException("-1", "请选择参与人");
        }
        String category = po.getCategory();
        if (category.equals("晚餐")) {
            String descr = po.getDescr();
            if (StringUtils.isEmpty(descr)) {
                throw new ReturnInfoException("-1", "请填写备注");
            }
        }
        HttpSession session = request.getSession();
        AdminUserPO sessionuser = (AdminUserPO) session.getAttribute("adminusersession");
        po.setCreateId(sessionuser.getId());
        return pocketBookSV.save(po);
    }

    @RequestMapping(value = "/del/{id}", method = RequestMethod.GET)
    public Object del(@PathVariable("id") Integer id) {
        return pocketBookSV.delById(id);
    }

    /*
    * 导出excel表格
    *
    * */
    @RequestMapping(value = "/export/xlsx",method = RequestMethod.GET)
    public void exportAll(HttpServletResponse response,String findTime){

        List<PocketBatch> pocketBatchList = new ArrayList<>();
        List<PocketBookPO> pocketBookPOList = pocketBookSV.getAllListByFindTime(findTime);
        Map<Integer,String> cachePayName = new HashMap<>();
        if(pocketBookPOList != null && pocketBookPOList.size()>0){
            for(PocketBookPO po : pocketBookPOList){
                PocketBatch pocketBatch = new PocketBatch();
                pocketBatch.setBuytime(po.getBuytime());
                pocketBatch.setCategory(po.getCategory());
                pocketBatch.setMoney(po.getMoney());
                pocketBatch.setBill(po.getBill());
                pocketBatch.setParticipant(po.getParticipant());
                pocketBatch.setDescr(po.getDescr());
                if(StringUtils.isEmpty(cachePayName.get(po.getCreateId()))){
                    String payName = adminUserSV.getById2(po.getCreateId()).getName();
                    pocketBatch.setPayName(payName);
                    cachePayName.put(po.getCreateId(),payName);
                }else pocketBatch.setPayName(cachePayName.get(po.getCreateId()));
                pocketBatchList.add(pocketBatch);
            }
        }
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/octet-stream");
        String fileName = findTime+"全部员工记账统计.xlsx";
        try(OutputStream os = response.getOutputStream();){
            fileName = new String(fileName.getBytes(),"ISO8859-1");
            response.setHeader("Content-Disposition","attachment;fileName="+fileName);
            BatchUilts.downBatch(pocketBatchList,os);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
