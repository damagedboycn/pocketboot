package com.damagedboy.pocketbook.utils.beans;

import java.util.Date;

public class PocketBatch {
    private Date buytime;
    private String category;
    private String payName;
    private Integer money;
    private Integer bill;
    private String participant;
    private String descr;


    @Override
    public String toString() {
        return "PocketBatch{" +
                "buytime=" + buytime +
                ", category='" + category + '\'' +
                ", payName='" + payName + '\'' +
                ", money=" + money +
                ", bill=" + bill +
                ", participant='" + participant + '\'' +
                ", descr='" + descr + '\'' +
                '}';
    }

    public Date getBuytime() {
        return buytime;
    }

    public void setBuytime(Date buytime) {
        this.buytime = buytime;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPayName() {
        return payName;
    }

    public void setPayName(String payName) {
        this.payName = payName;
    }

    public Integer getMoney() {
        return money;
    }

    public void setMoney(Integer money) {
        this.money = money;
    }

    public Integer getBill() {
        return bill;
    }

    public void setBill(Integer bill) {
        this.bill = bill;
    }

    public String getParticipant() {
        return participant;
    }

    public void setParticipant(String participant) {
        this.participant = participant;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }
}
