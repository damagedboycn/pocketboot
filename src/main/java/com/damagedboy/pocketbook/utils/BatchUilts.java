package com.damagedboy.pocketbook.utils;


import com.damagedboy.pocketbook.utils.beans.PocketBatch;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.IOException;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class BatchUilts {

/*    public static List<PocketBookPO> uploadBatch(InputStream is){
        List<PocketBookPO> itemBatches = new ArrayList<>();
        try {
            Workbook wb = WorkbookFactory.create(is);
            if (wb instanceof XSSFWorkbook) {
                XSSFWorkbook xWb = (XSSFWorkbook) wb;
                itemBatches = BatchUilts.getExcelInfo(xWb);
            } else if (wb instanceof HSSFWorkbook) {
                HSSFWorkbook hWb = (HSSFWorkbook) wb;
                itemBatches = BatchUilts.getExcelInfo(hWb);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return itemBatches;
    }*/

    public static Object downBatch(List<PocketBatch> pocketBatches, OutputStream outputStream){
        Workbook wb = new XSSFWorkbook();
        Sheet sheet = wb.createSheet();
        Row firstRow = sheet.createRow(0);
        //宽度的单位是1/256个字符宽度
        sheet.setColumnWidth(0,8*512);
        sheet.setColumnWidth(1,8*512);
        sheet.setColumnWidth(2,5*512);
        sheet.setColumnWidth(3,5*512);
        sheet.setColumnWidth(4,5*512);
        sheet.setColumnWidth(5,25*512);
        sheet.setColumnWidth(6,25*512);


        firstRow.createCell(0).setCellValue("日期");
        firstRow.createCell(1).setCellValue("类别（午餐晚餐）");
        firstRow.createCell(2).setCellValue("付账人");
        firstRow.createCell(3).setCellValue("消费金额");
        firstRow.createCell(4).setCellValue("发票金额");
        firstRow.createCell(5).setCellValue("参与人员");
        firstRow.createCell(6).setCellValue("备注");
        firstRow.setHeight((short)(3*256));

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        if(pocketBatches != null || pocketBatches.size()>0){
            for(int i=0;i < pocketBatches.size();i++){
                Row row = sheet.createRow(i+1);
                //Height的单位是1/20个点
                row.setHeight((short)(1*256));

                Cell buytimeCell = row.createCell(0);
                Cell categoryCell = row.createCell(1);
                Cell payNameCell = row.createCell(2);
                Cell moneyCell = row.createCell(3);
                Cell billCell = row.createCell(4);
                Cell participantCell = row.createCell(5);
                Cell descrCell = row.createCell(6);

                buytimeCell.setCellValue(simpleDateFormat.format(pocketBatches.get(i).getBuytime()));
                categoryCell.setCellValue(pocketBatches.get(i).getCategory());
                payNameCell.setCellValue(pocketBatches.get(i).getPayName());
                moneyCell.setCellValue(pocketBatches.get(i).getMoney());
                billCell.setCellValue(pocketBatches.get(i).getBill());
                participantCell.setCellValue(pocketBatches.get(i).getParticipant());
                descrCell.setCellValue(pocketBatches.get(i).getDescr());
            }
        };
        try {
            wb.write(outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

   /* private static List<ItemBatch> getExcelInfo(Workbook wb){
        List<ItemBatch> itemBatches = new ArrayList<>();
        //int sheetCounts = wb.getNumberOfSheets();
        //获取第一页的内容
        Sheet sheet = wb.getSheetAt(0);
        int lastRowNum = sheet.getLastRowNum();
        int firstRowNum = sheet.getFirstRowNum();
        System.out.println("firstRowNum"+firstRowNum);
        System.out.println("lastRowNum" +lastRowNum);
        Row row = null;
        for(int rowNum = firstRowNum + 1; rowNum <= lastRowNum; rowNum++){
            row = sheet.getRow(rowNum);
            if(row == null) continue;
            ItemBatch itemBatch = new ItemBatch();
            Cell titleCell = row.getCell(0);
            Cell keywordsCell = row.getCell(1);
            Cell authorNameCell = row.getCell(2);
            Cell contextCell = row.getCell(3);
            Cell categoryDescriptionCell = row.getCell(4);
            if(titleCell != null) itemBatch.setTitle(getCellValue(titleCell));
            if(keywordsCell != null) itemBatch.setKeywords(getCellValue(keywordsCell));
            if(authorNameCell != null) itemBatch.setAuthorName(getCellValue(authorNameCell));
            if(contextCell != null) itemBatch.setContext(getCellValue(contextCell));
            if(categoryDescriptionCell != null) itemBatch.setCategoryDescription(getCellValue(categoryDescriptionCell));
            if(titleCell == null && keywordsCell == null && authorNameCell == null && contextCell == null && categoryDescriptionCell == null)
                continue;
            itemBatches.add(itemBatch);
        }
        return itemBatches;
    }*/



    /**
     * 200 * 获取表格单元格Cell内容 201 * @param cell 202 * @return 203
     */
    private static String getCellValue(Cell cell) {

        String result = "";
        switch (cell.getCellType()) {
            case Cell.CELL_TYPE_NUMERIC:// 数字类型
                if (HSSFDateUtil.isCellDateFormatted(cell)) {// 处理日期格式、时间格式
                    SimpleDateFormat sdf = null;
                    if (cell.getCellStyle().getDataFormat() == HSSFDataFormat.getBuiltinFormat("h:mm")) {
                        sdf = new SimpleDateFormat("HH:mm");
                    } else {// 日期
                        sdf = new SimpleDateFormat("yyyy-MM-dd");
                    }
                    Date date = cell.getDateCellValue();
                    result = sdf.format(date);
                } else if (cell.getCellStyle().getDataFormat() == 58) {
                    // 处理自定义日期格式：m月d日(通过判断单元格的格式id解决，id的值是58)
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    double value = cell.getNumericCellValue();
                    Date date = org.apache.poi.ss.usermodel.DateUtil.getJavaDate(value);
                    result = sdf.format(date);
                } else {
                    double value = cell.getNumericCellValue();
                    CellStyle style = cell.getCellStyle();
                    DecimalFormat format = new DecimalFormat();
                    String temp = style.getDataFormatString();
                    // 单元格设置成常规
                    if (temp.equals("General")) {
                        format.applyPattern("#");
                    }
                    result = format.format(value);
                }
                break;
            case Cell.CELL_TYPE_STRING:// String类型
                result = cell.getRichStringCellValue().toString();
                break;
            case Cell.CELL_TYPE_BLANK:
                result = "";
                break;
            default:
                result = "";
                break;
        }
        return result;
    }

}
