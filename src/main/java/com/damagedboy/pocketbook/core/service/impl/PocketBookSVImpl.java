package com.damagedboy.pocketbook.core.service.impl;

import com.damagedboy.base.exception.RollBackExeption;
import com.damagedboy.base.utils.dao.ICommonDao;
import com.damagedboy.pocketbook.core.dao.IPocketBookDAO;
import com.damagedboy.pocketbook.core.po.PocketBookPO;
import com.damagedboy.pocketbook.core.service.IPocketBookSV;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.RollbackException;
import java.util.Date;
import java.util.List;

@Service
public class PocketBookSVImpl implements IPocketBookSV {
    @Autowired
    private IPocketBookDAO pocketBookDAO;
    @Autowired
    private ICommonDao<PocketBookPO> commonDao;

    @Override
    public List<PocketBookPO> getList(String findTime, Integer createId) {
        return pocketBookDAO.getList(findTime, createId);
    }

    @Override
    public PocketBookPO getById(Integer id) {
        return commonDao.getById("sys_pocket_book", id, PocketBookPO.class);
    }

    @Override
    @Transactional(readOnly = false, rollbackFor = RollbackException.class)
    public int save(PocketBookPO po) {
        try {
            Integer id = po.getId();
            if (id == null) {
                po.setCreateTime(new Date());
                return commonDao.insert("sys_pocket_book", po);
            }
            return id;
        } catch (Exception e) {
            throw new RollBackExeption(e);
        }
    }

    @Override
    @Transactional(readOnly = false, rollbackFor = RollbackException.class)
    public int delById(Integer id) {
        try {
            return commonDao.delById("sys_pocket_book", id);
        } catch (Exception e) {
            throw new RollBackExeption(e);
        }
    }

    @Override
    public List<PocketBookPO> getAllListByFindTime(String findTime) {
        return pocketBookDAO.getAllListByFindTime(findTime);
    }
}
