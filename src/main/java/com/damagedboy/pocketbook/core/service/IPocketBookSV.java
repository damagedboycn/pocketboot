package com.damagedboy.pocketbook.core.service;

import com.damagedboy.pocketbook.core.po.PocketBookPO;

import java.util.List;

public interface IPocketBookSV {
    List<PocketBookPO> getList(String findTime, Integer createId);

    PocketBookPO getById(Integer id);

    int save(PocketBookPO po);

    int delById(Integer id);

    List<PocketBookPO> getAllListByFindTime(String findTime);
}
