package com.damagedboy.pocketbook.core.dao.impl;

import com.damagedboy.pocketbook.core.dao.IPocketBookDAO;
import com.damagedboy.pocketbook.core.po.PocketBookPO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Repository
public class PocketBookDAOImpl implements IPocketBookDAO {
    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public List<PocketBookPO> getList(String findTime, Integer createId) {
        StringBuilder sb = new StringBuilder("select * from sys_pocket_book where 1=1 ");
        List<Object> params = new ArrayList<>();

        if (!StringUtils.isEmpty(findTime)) {
            sb.append("and buytime like ? ");
            params.add("%" + findTime + "%");
        }

        if (!StringUtils.isEmpty(createId)) {
            sb.append("and create_id = ? ");
            params.add(createId);
        }
        sb.append("order by buytime ");
        return jdbcTemplate.query(sb.toString(), params.toArray(), BeanPropertyRowMapper.newInstance(PocketBookPO.class));
    }

    @Override
    public List<PocketBookPO> getAllListByFindTime(String findTime) {
        String sql = "SELECT * from sys_pocket_book where buytime like ? ORDER BY create_id";

        if(!StringUtils.isEmpty(findTime)) findTime = findTime+"%";
        List<PocketBookPO> list = jdbcTemplate.query(sql,BeanPropertyRowMapper.newInstance(PocketBookPO.class),findTime);
        return list;
    }
}
