package com.damagedboy.pocketbook.core.dao;

import com.damagedboy.pocketbook.core.po.PocketBookPO;

import java.util.List;

public interface IPocketBookDAO {
    List<PocketBookPO> getList(String findTime, Integer createId);
    List<PocketBookPO> getAllListByFindTime(String findTime);
}
