package core;

import com.damagedboy.MyApplication;
import com.damagedboy.base.adminuser.IAdminUserSV;
import com.damagedboy.pocketbook.core.po.PocketBookPO;
import com.damagedboy.pocketbook.core.service.IPocketBookSV;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MyApplication.class)
public class PocketBookCoreTest {
    @Autowired
    IPocketBookSV core;
    @Autowired
    IAdminUserSV adminUserSV;

    @Test
    public void Test3(){
        System.out.println(core.getAllListByFindTime("2018-06"));
    }

    @Test
    public void Test4(){
        System.out.println(adminUserSV.getById2(9));
    }

    @Test
    public void  test5(){
        System.out.println(core.getList("2018-06",9));
    }

    @Test
    public void delTest(){
        System.out.println(core.delById(871));
    }

    @Test
    public void saveTest(){
        PocketBookPO po = new PocketBookPO();
        po.setBill(10000);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            po.setBuytime(simpleDateFormat.parse("2018-07-02"));
            po.setCategory("午餐");
            po.setDescr("测试程序");
            po.setMoney(100000);
            po.setParticipant("陈锦涛");
            po.setCreateId(25);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        core.save(po);
    }
}